# Battleship with JavaScript

We will be creating a simplified version of the classic game Battleship using JavaScript, HTML, and CSS. Traditionally the Battleship game has a grid of 7x7, we are going to start with just a 1x7 grid.

Code can be completed in [CodePen.](https://codepen.io) Create a free account and keep the code forever!

First, let's figure out the general flow of the game.

### The Flow

Here is an idea:

1. User starts the game

   * Game places a battleship at a random location on the grid.

2. Game play begins/ Repeat the following until the battleship is sunk:

   * Prompt user for a guess

   * Check the user's guess against the battleship to look for a hit, miss, or sink.

3. Game finishes. Give the user a rating based on the number of guesses. 

### More Details

There are a few more details we need to consider:

   * **The Battleship**: We know that the battleship takes up three consecutive cells out of seven. Let's call the location cell 1, 2, and 3. 

   * **User Input**: How will we get the user's input for the game? We can use an `prompt` function. The `prompt` will get a new location from the user and display a message and get the input.

   * **Showing the Results**: For the output, we will use `alert` to display the output of the game.

### Planning the Logic

Let's *psuedocode* our game logic. Pseudocode is a mix between real JS code and plain English of the program. 

First, let's think about what variables will be needed: 

   - **Declare** 3 variables to hold the location of the each cell of the ship. We can call them `location1`, `location2`, and `location3`.

   - **Declare** a variables to hold the user's current guess. We can call it `guess`.

   - **Declare** a variable to hold the number of hits. Call it `hits` and set it to 0.

   - **Declare** a variable to hold the number of guesses. Variable's name can be `guesses` and set to 0.

   - **Declare** a variable to keep track of the whether the battleship has sunk or not. Let's call it `isSunk` and set it to `false`.

   Next, let's psuedocode the logic.

   - LOOP: while the battleship is not sunk

      - GET: the user's guess

      - COMPARE the user's input to valid input values

      - IF the user's guess is invalid,
    
         - TELL user to enter a valid number

      - ELSE

         - ADD one to `guesses`

         - IF the user's guess matches a location

            - ADD one to the number of hits

            - IF number of hits is 3

               - SET `isSunk` to true

               - TELL user "You sank my battleship!"

            - END IF

        - END IF
    
     - END ELSE

- END LOOP

- TELL user stats

### Now for HTML

**OK**! Let's get the HTML set up. Be sure to have the proper `HTML` document structure. Add a heading for the Battleship game. Under the heading, link the `battleship.js` to the document using a `script` element. 

```html

<doctype html>
<html lang="en">
 <head>
    <title>BattleShip Game</title>
    <meta charset="utf-8">
</head>
<body>
    <h1>Some title</h1>
    <script src="battleship.js"></script>
</body>
</html>
```

### Setting up the Program

As stated before, we need to declare our variables. Let's do that:

```javascript

var location1 = 3;
var location2 = 4;
var location3 = 5;
var guess;
var hits = 0;
var guesses = 0;
var isSunk = false;
```

Next, we need to start our loop with a conditional test to determine whether to keep looping. Each time we go through the `while` loop, we are going to ask the user for a guess. 

```javascript

var location1 = 3;
var location2 = 4;
var location3 = 5;
var guess;
var hits = 0;
var guesses = 0;
var isSunk = false;

while (isSunk == false){
    guess = prompt("Ready, aim, fire! (enter a number from 0-6):")
}
```

Now we need to test the user's guess and keep a count of number of guesses. We will use `if/else` statements.

```javascript

var location1 = 3;
var location2 = 4;
var location3 = 5;
var guess;
var hits = 0;
var guesses = 0;
var isSunk = false;

while (isSunk == false){
    guess = prompt("Ready, aim, fire! (enter a number from 0-6):")

    if (guess < 0 || guess > 6) {
        alert("Please enter a valid cell number!");
    } else {
        guesses = guesses + 1; 
        }
    }

```

Alright, now we need to see if the user's guess hit the battleship and count the number of hits. Also send an `alert` if the battleship was hit. 

```javascript

var location1 = 3;
var location2 = 4;
var location3 = 5;
var guess;
var hits = 0;
var guesses = 0;
var isSunk = false;

while (isSunk == false){
    guess = prompt("Ready, aim, fire! (enter a number from 0-6):")

    if (guess < 0 || guess > 6) {
        alert("Please enter a valid cell number!");
    } else {
        guesses = guesses + 1; 
        }

    if (guess == location1 || guess == location2 || guess == location3) { 
            alert("HIT!");
            hits = hits + 1; 
                if (hits == 3) {
                    isSunk = true;
                    alert("You sank my battleship!"); 
                }
    }   else {
            alert("MISS");
         }
}

```

Lastly, display the stats to the user.


```javascript

var location1 = 3;
var location2 = 4;
var location3 = 5;
var guess;
var hits = 0;
var guesses = 0;
var isSunk = false;

while (isSunk == false){
    guess = prompt("Ready, aim, fire! (enter a number from 0-6):")

    if (guess < 0 || guess > 6) {
        alert("Please enter a valid cell number!");
    } else {
        guesses = guesses + 1; 
        }

    if (guess == location1 || guess == location2 || guess == location3) { 
            alert("HIT!");
            hits = hits + 1; 
                if (hits == 3) {
                    isSunk = true;
                    alert("You sank my battleship!"); 
                }
    }   else {
            alert("MISS");
         }
}

    var stats = "You took " + guesses + " guesses to sink the battleship, " +
    "which means your shooting accuracy was " + (3/guesses); 
    alert(stats);

```

### You now have a simplified version of the Battleship game. Have fun!